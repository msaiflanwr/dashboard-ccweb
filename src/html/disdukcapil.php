<?php

include ('connect-db.php');

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/cc-icon2.png">
    <title>DISDUKCAPIL - Command Center Kabupaten Tasikmalaya | Monitoring Apps</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <!-- Custom CSS -->
    <link href="../dist/css/style.min.css" rel="stylesheet">
    <!-- This Page CSS -->
    <link href="../assets/libs/morris.js/morris.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <!-- Logo icon -->
                        <a href="index.php">
                            <b class="logo-icon">
                                <!-- Dark Logo icon -->
                                <img src="../assets/images/logo-icon2.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo icon -->
                                <img src="../assets/images/logo-icon2.png" alt="homepage" class="light-logo" />
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text -->
                            <span class="logo-text">
                                <!-- dark Logo text -->
                                <img src="../assets/images/cc-text.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo text -->
                                <img src="../assets/images/logo-light-text.png" class="light-logo" alt="homepage" />
                            </span>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto ml-3 pl-1">
                        <!-- Notification -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle pl-md-3 position-relative" href="javascript:void(0)"
                                id="bell" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <span><i data-feather="bell" class="svg-icon"></i></span>
                                <span class="badge badge-primary notify-no rounded-circle">5</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left mailbox animated bounceInDown">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="message-center notifications position-relative">
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <div class="btn btn-danger rounded-circle btn-circle"><i
                                                        data-feather="airplay" class="text-white"></i></div>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Menu Admin</h6>
                                                    <span class="font-12 text-nowrap d-block text-muted">Package Flutter Terbaru</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:30 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-success text-white rounded-circle btn-circle"><i
                                                        data-feather="calendar" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Kegiatan Hari Ini</h6>
                                                    <span
                                                        class="font-12 text-nowrap d-block text-muted text-truncate">Beberapa Kegiatan Hari Ini Ditambahkan!</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:10 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-info rounded-circle btn-circle"><i
                                                        data-feather="settings" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Settings</h6>
                                                    <span
                                                        class="font-12 text-nowrap d-block text-muted text-truncate">Perbaharui Email Anda</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:08 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-primary rounded-circle btn-circle"><i
                                                        data-feather="box" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Surat Masuk</h6> <span
                                                        class="font-12 text-nowrap d-block text-muted">Cek Surat Masuk dari Desa Margasari</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link pt-3 text-center text-dark" href="javascript:void(0);">
                                            <strong>Check all notifications</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- End Notification -->
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i data-feather="settings" class="svg-icon"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Pengaturan</a>
                                <a class="dropdown-item" href="#">Tampilan</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Default Settings</a>
                            </div>
                        </li>
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link" href="javascript:void(0)">
                                <div class="customize-input">
                                    <select
                                        class="custom-select form-control bg-white custom-radius custom-shadow border-0">
                                        <option selected>ID</option>
                                        <option value="1">EN</option>
                                        <option value="2">US</option>
                                    </select>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link" href="javascript:void(0)">
                                <form>
                                    <div class="customize-input">
                                        <input class="form-control custom-shadow custom-radius border-0 bg-white"
                                            type="search" placeholder="Search" aria-label="Search">
                                        <i class="form-control-icon" data-feather="search"></i>
                                    </div>
                                </form>
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <!-- <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="../assets/images/users/saiful2.png" alt="user" class="rounded-circle"
                                    width="40">
                                <span class="ml-2 d-none d-lg-inline-block"><span>Halo,</span> <span
                                        class="text-dark">M. Saiful</span> <i data-feather="chevron-down"
                                        class="svg-icon"></i></span>
                            </a> -->
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="user"
                                        class="svg-icon mr-2 ml-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="credit-card"
                                        class="svg-icon mr-2 ml-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="mail"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="settings"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="power"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Logout</a>
                                <div class="dropdown-divider"></div>
                                <div class="pl-4 p-3"><a href="javascript:void(0)" class="btn btn-sm btn-info">View
                                        Profile</a></div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="index.php"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">SKPD</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="setda.php"
                                aria-expanded="false"><img src="../assets/images/icon/setda/setda.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">Sekretariat Daerah
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dprd.php"
                                aria-expanded="false"><img src="../assets/images/icon/dprd/dprd.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">Sekretariat DPRD</span></a></li>
                                    <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><img src="../assets/images/icon/inspektorat/inspektorat.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">Inspektorat </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="inspektorat.php" class="sidebar-link"><span
                                            class="hide-menu"> SKPD
                                        </span>    
                             <li class="sidebar-item"><a href="inspektorat-kecamatan.php" class="sidebar-link"><span
                                            class="hide-menu"> Kecamatan
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="inspektorat-desa.php" class="sidebar-link"><span
                                            class="hide-menu"> Desa
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="disdikbud.php"
                                aria-expanded="false"><img src="../assets/images/icon/disdikbud/disdikbud.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISDIKBUD</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="dinkes.php"
                                aria-expanded="false"><img src="../assets/images/icon/dinkes/dinkes.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DINAS KESEHATAN</span></a>
                                    <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                    <li class="sidebar-item"><a href="dinkes.php" class="sidebar-link"><span
                                                class="hide-menu"> Infokes
                                            </span>    
                                 <li class="sidebar-item"><a href="dinkes-sigizi.php" class="sidebar-link"><span
                                                class="hide-menu"> Sigizi
                                            </span></a>
                                    </li>
                                </ul>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dputr.php"
                                aria-expanded="false"><img src="../assets/images/icon/dputr/dputr.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DPUTR-PRKPL</span></a></li>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="satpolpp.php"
                            aria-expanded="false"><img src="../assets/images/icon/satpolpp/satpolpp.svg" style="height: 30px;" class="feather-icon"></img><span
                                class="hide-menu">SATPOL PP</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dinsos.php"
                                aria-expanded="false"><img src="../assets/images/icon/dinsos/dinsos.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DINSOS PPKB P3A</span></a>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="disparpora.php"
                                aria-expanded="false"><img src="../assets/images/icon/disparpora/disparpora.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISPARPORA</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="diskopukmindag.php"
                                aria-expanded="false"><img src="../assets/images/icon/diskopukmindag/diskopukmindag.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISKOPUKMINDAG</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="disperpakan.php"
                                aria-expanded="false"><img src="../assets/images/icon/pertanian/pertanian.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISPERPAKAN</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="disdukcapil.php"
                                aria-expanded="false"><img src="../assets/images/icon/disdukcapil/disdukcapil-white.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISDUKCAPIL</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dpmptsp.php"
                                aria-expanded="false"><img src="../assets/images/icon/dpmptsp/dpmptsp.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DPMPTSPTK</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dishubkominfo.php"
                                aria-expanded="false"><img src="../assets/images/icon/diskominfo/diskominfo.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISHUBKOMINFO</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="bappelitbangda.php"
                                aria-expanded="false"><img src="../assets/images/icon/bappeda/bappeda.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">BAPPELITBANGDA</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="bkpsdm.php"
                                aria-expanded="false"><img src="../assets/images/icon/bkpsdm/bkpsdm.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">BKPSDM</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="bpkpd.php"
                                aria-expanded="false"><img src="../assets/images/icon/bpkpd/bpkpd.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">BPKPD</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="bpbd.php"
                                aria-expanded="false"><img src="../assets/images/icon/bpbd/bpbd.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">BPBD</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="kesbangpol.php"
                                aria-expanded="false"><img src="../assets/images/icon/kesbangpol/kesbangpol.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">KESBANGPOL</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="smc.php"
                                aria-expanded="false"><img src="../assets/images/icon/smc/smc.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">RUMAH SAKIT SMC</span></a></li>
                            <br>
                            <br>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="table-basic.html" class="sidebar-link"><span
                                            class="hide-menu"> Basic Table
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="table-dark-basic.html" class="sidebar-link"><span
                                            class="hide-menu"> Dark Basic Table
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="table-sizing.html" class="sidebar-link"><span
                                            class="hide-menu">
                                            Sizing Table
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="table-layout-coloured.html" class="sidebar-link"><span
                                            class="hide-menu">
                                            Coloured
                                            Table Layout
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="table-datatable-basic.html" class="sidebar-link"><span
                                            class="hide-menu">
                                            Basic
                                            Datatables
                                            Layout
                                        </span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Dinas Kependudukan dan Pencatatan Sipil Kab. Tasikmalaya</h3>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.php" class="text-muted">Dashboard</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Data DISDUKCAPIL</li>
                                    <br>
                                    <br>
                                </ol>
                                <h6 class="text">Update Terakhir : Selasa, 10 Januari 2023 | Pukul 14.39 WIB</h6>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <!-- *************************************************************** -->
                <!-- Start First Cards -->
                <!-- *************************************************************** -->
                <div class="card-group">
                <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">656.953</h2>
                                        <span
                                            class="badge bg-success font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">2021</span>
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Jumlah Keluarga Berdasarkan KK</h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <span class="opacity-7 text-muted"><i data-feather="users"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">1.865.898</h2>
                                        <span
                                        class="badge bg-success font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">2021</span>
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Jumlah Penduduk
                                    </h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <span class="opacity-7 text-muted"><i data-feather="pie-chart"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">1.283.014</h2>
                                        <span
                                        class="badge bg-success font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">2021</span>
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Jumlah Penduduk Berdasarkan Akta Nikah
                                    </h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <span class="opacity-7 text-muted"><i data-feather="users"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border-right">
                        <div class="card-body">
                            <div class="d-flex d-lg-flex d-md-block align-items-center">
                                <div>
                                    <div class="d-inline-flex align-items-center">
                                        <h2 class="text-dark mb-1 font-weight-medium">12.769</h2>
                                        <span
                                        class="badge bg-danger font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">2021</span>
                                    </div>
                                    <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Jumlah Penduduk Berdasarkan Akta Kematian
                                    </h6>
                                </div>
                                <div class="ml-auto mt-md-3 mt-lg-0">
                                    <span class="opacity-7 text-muted"><i data-feather="activity"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk Berdasarkan Usia Sekolah</h4>
                                <h6 class="card-subtitle">Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div>
                                    <canvas id="chart-usia-sekolah" height="150"> </canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Grafik Jumlah Penduduk</h4>
                                <h6 class="card-subtitle">Kabupaten Tasikmalaya Tahun 2019 - 2021</h6>
                                <div>
                                    <canvas id="chart-jumlah-penduduk" height="150"> </canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk Berdasarkan Jenis Kelamin Menurut Kelompok Usia</h4>
                                <h6 class="card-subtitle">Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div>
                                    <canvas id="chart-kelompok-usia-jk" height="150"> </canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk Berdasarkan Agama dan Jenis Kelamin</h4>
                                <h6 class="card-subtitle">Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div id="chart-bangunan-pasar"></div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk Berdasarkan Kepemilikan Akta Nikah</h4>
                                <h6 class="card-subtitle">Menurut Kecamatan di Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div class="table-responsive">
                                <table class="table table-sm">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <tr>
                                                <th rowspan="2" class="center" style="vertical-align:middle">No.</th>
                                                <th rowspan="2" class="center" style="vertical-align:middle">Kecamatan</th>
                                                <th colspan="2" class="center" style="text-align:center">Kepemilikan Akta Nikah</th>
                                                <th colspan="2" rowspan="2" class="center" style="text-align: center; vertical-align: middle;">Jumlah</th>
                                                </tr>
                                                <tr>
                                                    <th class="center" style="text-align:center">Memiliki Akta Nikah</th>
                                                    <th class="center" style="text-align:center">Belum Memiliki Akta Nikah</th>
                                                </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            while($row = mysqli_fetch_array($result92))
                                            {
                                            echo '<tr>
                                                <td class="center" style="text-align:center">'.$row['id'].'</td>
                                                <td class="center" style="text-align:center"><b>'.$row['Kecamatan'].'</b></td>
                                                <td style="text-align:center">'.$row['Sudah_KK'].'</td>
                                                <td style="text-align:center">'.$row['Belum_KK'].'</td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['Jumlah'].'</font></b></td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk Berdasarkan Kepemilikan Akta Kelahiran</h4>
                                <h6 class="card-subtitle">Menurut Kecamatan di Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div class="table-responsive">
                                <table class="table table-sm">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <tr>
                                                <th rowspan="2" class="center" style="vertical-align:middle">No.</th>
                                                <th rowspan="2" class="center" style="vertical-align:middle">Kecamatan</th>
                                                <th colspan="2" class="center" style="text-align:center">Kepemilikan Akta Kelahiran</th>
                                                <th colspan="2" rowspan="2" class="center" style="text-align: center; vertical-align: middle;">Jumlah</th>
                                                </tr>
                                                <tr>
                                                    <th class="center" style="text-align:center">Memiliki Akta Kelahiran</th>
                                                    <th class="center" style="text-align:center">Belum Memiliki Akta Kelahiran</th>
                                                </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            while($row = mysqli_fetch_array($result94))
                                            {
                                            echo '<tr>
                                                <td class="center" style="text-align:center">'.$row['id'].'</td>
                                                <td class="center" style="text-align:center"><b>'.$row['Kecamatan'].'</b></td>
                                                <td style="text-align:center">'.$row['sudah_akta_lahir'].'</td>
                                                <td style="text-align:center">'.$row['belum_akta_lahir'].'</td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['Jumlah'].'</font></b></td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Keluarga Berdasarkan Kepemilikan Kartu Keluarga</h4>
                                <h6 class="card-subtitle">Menurut Kecamatan di Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div class="table-responsive">
                                <table class="table table-sm">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <tr>
                                                <th rowspan="2" class="center" style="vertical-align:middle">No.</th>
                                                <th rowspan="2" class="center" style="vertical-align:middle">Kecamatan</th>
                                                <th colspan="2" class="center" style="text-align:center">Kepemilikan Kartu Keluarga</th>
                                                <th colspan="2" rowspan="2" class="center" style="text-align: center; vertical-align: middle;">Jumlah</th>
                                                </tr>
                                                <tr>
                                                    <th class="center" style="text-align:center">Memiliki Kartu Keluarga</th>
                                                    <th class="center" style="text-align:center">Belum Memiliki Kartu Keluarga</th>
                                                </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            while($row = mysqli_fetch_array($result96))
                                            {
                                            echo '<tr>
                                                <td class="center" style="text-align:center">'.$row['id'].'</td>
                                                <td class="center" style="text-align:center"><b>'.$row['Kecamatan'].'</b></td>
                                                <td style="text-align:center">'.$row['sudah_kk'].'</td>
                                                <td style="text-align:center">'.$row['belum_kk'].'</td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['Jumlah'].'</font></b></td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Keluarga Berdasarkan Kepemilikan Akta Kematian</h4>
                                <h6 class="card-subtitle">Menurut Kecamatan di Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div class="table-responsive">
                                <table class="table table-sm">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <tr>
                                                <th rowspan="2" class="center" style="vertical-align:middle">No.</th>
                                                <th rowspan="2" class="center" style="vertical-align:middle">Kecamatan</th>
                                                <th colspan="2" class="center" style="text-align:center">Kepemilikan Akta Kematian</th>
                                                <th colspan="2" rowspan="2" class="center" style="text-align: center; vertical-align: middle;">Jumlah</th>
                                                </tr>
                                                <tr>
                                                    <th class="center" style="text-align:center">Memiliki Akta Kematian</th>
                                                    <th class="center" style="text-align:center">Belum Memiliki Akta Kematian</th>
                                                </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            while($row = mysqli_fetch_array($result97))
                                            {
                                            echo '<tr>
                                                <td class="center" style="text-align:center">'.$row['id'].'</td>
                                                <td class="center" style="text-align:center"><b>'.$row['Kecamatan'].'</b></td>
                                                <td style="text-align:center">'.$row['sudah_akta_kematian'].'</td>
                                                <td style="text-align:center">'.$row['belum_akta_kematian'].'</td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['jumlah'].'</font></b></td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk Berdasarkan Jenis Kelamin dan Rasio Jenis Kelamin</h4>
                                <h6 class="card-subtitle">Menurut Kecamatan di Kabupaten Tasikmalaya Tahun 2019 - 2021</h6>
                                <div class="table-responsive">
                                <table class="table table-sm">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <tr>
                                                <th rowspan="3" class="center" style="vertical-align:middle">No.</th>
                                                <th rowspan="3" class="center" style="vertical-align:middle">Kecamatan</th>
                                                <th colspan="9" class="center" style="text-align:center">Jenis Kelamin</th>
                                                <th colspan="3" rowspan="2" class="center" style="text-align:center">Rasio Jenis Kelamin</th>
                                                </tr>
                                                <tr>
                                                <th colspan="3" class="center" style="text-align:center">Laki - Laki</th>
                                                <th colspan="3" class="center" style="text-align:center">Perempuan</th>
                                                <th colspan="3" class="center" style="text-align:center">Jumlah</th>
                                                </tr>
                                                <tr>
                                                    <th class="center" style="text-align:center">2019</th>
                                                    <th class="center" style="text-align:center">2020</th>
                                                    <th class="center" style="text-align:center">2021</th>
                                                    <th class="center" style="text-align:center">2019</th>
                                                    <th class="center" style="text-align:center">2020</th>
                                                    <th class="center" style="text-align:center">2021</th>
                                                    <th class="center" style="text-align:center">2019</th>
                                                    <th class="center" style="text-align:center">2020</th>
                                                    <th class="center" style="text-align:center">2021</th>
                                                    <th class="center" style="text-align:center">2019</th>
                                                    <th class="center" style="text-align:center">2020</th>
                                                    <th class="center" style="text-align:center">2021</th>                                                    
                                                </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            while($row = mysqli_fetch_array($result100))
                                            {
                                            echo '<tr>
                                                <td class="center" style="text-align:center">'.$row['id'].'</td>
                                                <td class="center" style="text-align:center"><b>'.$row['Kecamatan'].'</b></td>
                                                <td style="text-align:center">'.$row['l_2019'].'</td>
                                                <td style="text-align:center">'.$row['l_2020'].'</td>
                                                <td style="text-align:center">'.$row['l_2021'].'</td>
                                                <td style="text-align:center">'.$row['p_2019'].'</td>
                                                <td style="text-align:center">'.$row['p_2020'].'</td>
                                                <td style="text-align:center">'.$row['p_2021'].'</td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['jumlah_2019'].'</font></b></td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['jumlah_2020'].'</font></b></td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['jumlah_2021'].'</font></b></td>
                                                <td style="text-align:center">'.$row['rasio_2019'].'</td>
                                                <td style="text-align:center">'.$row['rasio_2020'].'</td>
                                                <td style="text-align:center">'.$row['rasio_2021'].'</td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk, Persentase Penduduk dan Kepadatan Penduduk</h4>
                                <h6 class="card-subtitle">Menurut Kecamatan di Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div class="table-responsive">
                                <table class="table table-sm">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <tr>
                                                    <th class="center" style="text-align:center">No.</th>
                                                    <th class="center" style="text-align:center">Kecamatan</th>
                                                    <th class="center" style="text-align:center">Jumlah Penduduk (orang)</th>
                                                    <th class="center" style="text-align:center">Persentase Penduduk (%)</th>
                                                    <th class="center" style="text-align:center">Kepadatan Penduduk (orang per km²)</th>
                                                </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            while($row = mysqli_fetch_array($result95))
                                            {
                                            echo '<tr>
                                                <td class="center" style="text-align:center">'.$row['id'].'</td>
                                                <td class="center" style="text-align:center"><b>'.$row['Kecamatan'].'</b></td>
                                                <td style="text-align:center">'.$row['penduduk'].'</td>
                                                <td style="text-align:center">'.$row['persentase'].'</td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['kepadatan'].'</font></b></td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk Berdasarkan Kewajiban Kepemilikan Kartu Identitas Anak (KIA) dan Jenis Kelamin</h4>
                                <h6 class="card-subtitle">Menurut Kecamatan di Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div class="table-responsive">
                                <table class="table table-sm">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <tr>
                                                <th rowspan="2" class="center" style="vertical-align:middle">No.</th>
                                                <th rowspan="2" class="center" style="vertical-align:middle">Kecamatan</th>
                                                <th colspan="3" class="center" style="text-align:center">Wajib KIA</th>
                                                <th colspan="4" class="center" style="text-align:center">Memiliki KIA</th>
                                                <th colspan="4" class="center" style="text-align:center">Belum Memiliki KIA</th>
                                                </tr>
                                                <tr>
                                                    <th class="center" style="text-align:center">Laki - Laki</th>
                                                    <th class="center" style="text-align:center">Perempuan</th>
                                                    <th class="center" style="text-align:center">Jumlah</th>
                                                    <th class="center" style="text-align:center">Laki - Laki</th>
                                                    <th class="center" style="text-align:center">Perempuan</th>
                                                    <th class="center" style="text-align:center">Jumlah</th>
                                                    <th class="center" style="text-align:center">%</th>
                                                    <th class="center" style="text-align:center">Laki - Laki</th>
                                                    <th class="center" style="text-align:center">Perempuan</th>
                                                    <th class="center" style="text-align:center">Jumlah</th>
                                                    <th class="center" style="text-align:center">%</th>
                                                </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            while($row = mysqli_fetch_array($result98))
                                            {
                                            echo '<tr>
                                                <td class="center" style="text-align:center">'.$row['id'].'</td>
                                                <td class="center" style="text-align:center"><b>'.$row['Kecamatan'].'</b></td>
                                                <td style="text-align:center">'.$row['wajib_l'].'</td>
                                                <td style="text-align:center">'.$row['wajib_p'].'</td>
                                                <td style="text-align:center"><b>'.$row['wajib_jumlah'].'</b></td>
                                                <td style="text-align:center">'.$row['memiliki_l'].'</td>
                                                <td style="text-align:center">'.$row['memiliki_p'].'</td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['memiliki_jumlah'].'</font></b></td>
                                                <td style="text-align:center">'.$row['memiliki_persentase'].'</td>
                                                <td style="text-align:center">'.$row['belum_l'].'</td>
                                                <td style="text-align:center">'.$row['belum_p'].'</td>
                                                <td style="text-align:center"><b><font color="#FF0000">'.$row['belum_jumlah'].'</font></b></td>
                                                <td style="text-align:center">'.$row['belum_persentase'].'</td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Jumlah Penduduk Berdasarkan Kewajiban Kepemilikan Kartu Tanda Penduduk (KTP) dan Jenis Kelamin</h4>
                                <h6 class="card-subtitle">Menurut Kecamatan di Kabupaten Tasikmalaya Tahun 2021</h6>
                                <div class="table-responsive">
                                <table class="table table-sm">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <tr>
                                                <th rowspan="2" class="center" style="vertical-align:middle">No.</th>
                                                <th rowspan="2" class="center" style="vertical-align:middle">Kecamatan</th>
                                                <th colspan="3" class="center" style="text-align:center">Wajib KTP-EL</th>
                                                <th colspan="4" class="center" style="text-align:center">Memiliki KTP-EL</th>
                                                <th colspan="4" class="center" style="text-align:center">Belum Memiliki KTP-EL</th>
                                                </tr>
                                                <tr>
                                                    <th class="center" style="text-align:center">Laki - Laki</th>
                                                    <th class="center" style="text-align:center">Perempuan</th>
                                                    <th class="center" style="text-align:center">Jumlah</th>
                                                    <th class="center" style="text-align:center">Laki - Laki</th>
                                                    <th class="center" style="text-align:center">Perempuan</th>
                                                    <th class="center" style="text-align:center">Jumlah</th>
                                                    <th class="center" style="text-align:center">%</th>
                                                    <th class="center" style="text-align:center">Laki - Laki</th>
                                                    <th class="center" style="text-align:center">Perempuan</th>
                                                    <th class="center" style="text-align:center">Jumlah</th>
                                                    <th class="center" style="text-align:center">%</th>
                                                </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            while($row = mysqli_fetch_array($result99))
                                            {
                                            echo '<tr>
                                                <td class="center" style="text-align:center">'.$row['id'].'</td>
                                                <td class="center" style="text-align:center"><b>'.$row['Kecamatan'].'</b></td>
                                                <td style="text-align:center">'.$row['wajib_l'].'</td>
                                                <td style="text-align:center">'.$row['wajib_p'].'</td>
                                                <td style="text-align:center"><b>'.$row['wajib_jumlah'].'</b></td>
                                                <td style="text-align:center">'.$row['memiliki_l'].'</td>
                                                <td style="text-align:center">'.$row['memiliki_p'].'</td>
                                                <td style="text-align:center"><b><font color="#76BA1B">'.$row['memiliki_jumlah'].'</font></b></td>
                                                <td style="text-align:center">'.$row['memiliki_persentase'].'</td>
                                                <td style="text-align:center">'.$row['belum_l'].'</td>
                                                <td style="text-align:center">'.$row['belum_p'].'</td>
                                                <td style="text-align:center"><b><font color="#FF0000">'.$row['belum_jumlah'].'</font></b></td>
                                                <td style="text-align:center">'.$row['belum_persentase'].'</td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center text-muted">
                All Rights Reserved by Command Center Kabupaten Tasikmalaya. <br> Designed and Developed by <a
                    href="https://instagram.com/commandcenter_kabtasik">DISHUBKOMINFO</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="../dist/js/app-style-switcher.js"></script>
    <script src="../dist/js/feather.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <!-- themejs -->
    <!--Menu sidebar -->
    <script src="../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <!--Morris JavaScript -->
    <script src="../assets/libs/raphael/raphael.min.js"></script>
    <script src="../assets/libs/morris.js/morris.min.js"></script>
    <script src="../dist/js/pages/morris/morris-data.js"></script>
    <!-- Chart JS -->
    <script src="../dist/js/pages/chartjs/chartjs.init.js"></script>
    <script src="../assets/libs/chart.js/dist/Chart.min.js"></script>
    <!-- Script Chart Langsung -->
    <script>

        Morris.Bar({

            element : 'chart-bangunan-pasar',
            data: [<?php echo $chart_data93; ?>],
            xkey: 'agama',
            ykeys: ['laki_laki','perempuan'],
            labels: ['laki Laki','Perempuan'],
            hideHover: 'auto',
            resize: true,
            gridTextSize: 9,
            barRatio: 0.4,
            xLabelAngle: 30,
            barColors: ["#00589C","#3DC6C3"]

        });

        new Chart(document.getElementById("chart-usia-sekolah"), {
            type: 'horizontalBar',
            data: {
            labels: [
                "Perguruan Tinggi (19-24)",
                "SMA/Sederajat (16-18)",
                "SMP/Sederajat (13-15)",
                "SD/Sederajat (7-12)",
                "Pra Sekolah (0-6)"   
            ],
            datasets: [
                {
                    label: "Jumlah (Dalam Satuan Orang)",
                    backgroundColor: ["#00589C", "#016FC4", "#1891C3", "#3AC0DA", "#3DC6C3"],
                    data: [191652,70879,95784,191884,201129]
                    }
            ]
            },
            options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Kabupaten Tasikmalaya Tahun 2021'
            }
            }
        });

        new Chart(document.getElementById("chart-jumlah-penduduk"), {
            type: 'horizontalBar',
            data: {
            labels: [
                "2021",
                "2020",
                "2019",   
            ],
            datasets: [
                {
                    label: "Jumlah (Dalam Satuan Orang)",
                    backgroundColor: ["#00589C", "#3AC0DA", "#50E3C2"],
                    data: [1865989,1803800,1796496]
                    }
            ]
            },
            options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Kabupaten Tasikmalaya Tahun 2019 - 2021'
            }
            }
        });

        new Chart(document.getElementById("chart-kelompok-usia-jk"), {
            type: 'horizontalBar',
            data: {
            labels: [
                "0-4 Tahun",
                "5-9 Tahun",
                "10-14 Tahun",
                "15-19 Tahun",
                "20-24 Tahun",
                "25-29 Tahun",
                "30-34 Tahun",
                "35-39 Tahun",
                "40-44 Tahun",
                "45-49 Tahun",
                "50-54 Tahun",
                "55-59 Tahun",
                "60-64 Tahun",
                "65-69 Tahun",
                "70-75 Tahun",
                ">75 Tahun"
            ],
            datasets: [
                {
                    axis: 'y',
                    label: "Laki - Laki",
                    backgroundColor: ["#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C", "#00589C"],
                    data: [
                            72639,
                            80159,
                            82988,
                            68495,
                            82416,
                            77156,
                            63725,
                            70016,
                            62284,
                            64871,
                            60590,
                            53641,
                            40361,
                            28373,
                            17668,
                            22945
                        ]
                    },
                {
                    axis: 'y',
                    label: "Perempuan",
                    backgroundColor: ["#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2", "#50E3C2"],
                    data: [
                            67855,
                            74928,
                            78724,
                            65905,
                            77219,
                            70815,
                            61268,
                            67560,
                            61740,
                            67174,
                            59838,
                            53197,
                            39024,
                            27977,
                            18653,
                            25694
                        ]
                    }
                ]
            },
            options: {
            legend: { display: true,
                position: 'bottom',
                labels: {
                    fontColor: "#000000"
                }        
            },
            title: {
                display: true,
                text: 'Kelompok Usia'
            }
            }
        });

    </script>
</body>

</html>