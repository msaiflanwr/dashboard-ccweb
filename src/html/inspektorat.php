<?php

include ('connect-db.php');

?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/cc-icon2.png">
    <title>Inspektorat (SKPD) - Command Center Kabupaten Tasikmalaya | Monitoring Apps</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <!-- Custom CSS -->
    <link href="../dist/css/style.min.css" rel="stylesheet">
    <!-- This Page CSS -->
    <link href="../assets/libs/morris.js/morris.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <!-- Logo icon -->
                        <a href="index.php">
                            <b class="logo-icon">
                                <!-- Dark Logo icon -->
                                <img src="../assets/images/logo-icon2.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo icon -->
                                <img src="../assets/images/logo-icon2.png" alt="homepage" class="light-logo" />
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text -->
                            <span class="logo-text">
                                <!-- dark Logo text -->
                                <img src="../assets/images/cc-text.png" alt="homepage" class="dark-logo" />
                                <!-- Light Logo text -->
                                <img src="../assets/images/logo-light-text.png" class="light-logo" alt="homepage" />
                            </span>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                        data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto ml-3 pl-1">
                        <!-- Notification -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle pl-md-3 position-relative" href="javascript:void(0)"
                                id="bell" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <span><i data-feather="bell" class="svg-icon"></i></span>
                                <span class="badge badge-primary notify-no rounded-circle">5</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left mailbox animated bounceInDown">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="message-center notifications position-relative">
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <div class="btn btn-danger rounded-circle btn-circle"><i
                                                        data-feather="airplay" class="text-white"></i></div>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Menu Admin</h6>
                                                    <span class="font-12 text-nowrap d-block text-muted">Package Flutter Terbaru</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:30 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-success text-white rounded-circle btn-circle"><i
                                                        data-feather="calendar" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Kegiatan Hari Ini</h6>
                                                    <span
                                                        class="font-12 text-nowrap d-block text-muted text-truncate">Beberapa Kegiatan Hari Ini Ditambahkan!</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:10 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-info rounded-circle btn-circle"><i
                                                        data-feather="settings" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Settings</h6>
                                                    <span
                                                        class="font-12 text-nowrap d-block text-muted text-truncate">Perbaharui Email Anda</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:08 AM</span>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)"
                                                class="message-item d-flex align-items-center border-bottom px-3 py-2">
                                                <span class="btn btn-primary rounded-circle btn-circle"><i
                                                        data-feather="box" class="text-white"></i></span>
                                                <div class="w-75 d-inline-block v-middle pl-2">
                                                    <h6 class="message-title mb-0 mt-1">Surat Masuk</h6> <span
                                                        class="font-12 text-nowrap d-block text-muted">Cek Surat Masuk dari Desa Margasari</span>
                                                    <span class="font-12 text-nowrap d-block text-muted">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link pt-3 text-center text-dark" href="javascript:void(0);">
                                            <strong>Check all notifications</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- End Notification -->
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i data-feather="settings" class="svg-icon"></i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Pengaturan</a>
                                <a class="dropdown-item" href="#">Tampilan</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Default Settings</a>
                            </div>
                        </li>
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link" href="javascript:void(0)">
                                <div class="customize-input">
                                    <select
                                        class="custom-select form-control bg-white custom-radius custom-shadow border-0">
                                        <option selected>ID</option>
                                        <option value="1">EN</option>
                                        <option value="2">US</option>
                                    </select>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link" href="javascript:void(0)">
                                <form>
                                    <div class="customize-input">
                                        <input class="form-control custom-shadow custom-radius border-0 bg-white"
                                            type="search" placeholder="Search" aria-label="Search">
                                        <i class="form-control-icon" data-feather="search"></i>
                                    </div>
                                </form>
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <!-- <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <img src="../assets/images/users/saiful2.png" alt="user" class="rounded-circle"
                                    width="40">
                                <span class="ml-2 d-none d-lg-inline-block"><span>Halo,</span> <span
                                        class="text-dark">M. Saiful</span> <i data-feather="chevron-down"
                                        class="svg-icon"></i></span>
                            </a> -->
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="user"
                                        class="svg-icon mr-2 ml-1"></i>
                                    My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="credit-card"
                                        class="svg-icon mr-2 ml-1"></i>
                                    My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="mail"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="settings"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i data-feather="power"
                                        class="svg-icon mr-2 ml-1"></i>
                                    Logout</a>
                                <div class="dropdown-divider"></div>
                                <div class="pl-4 p-3"><a href="javascript:void(0)" class="btn btn-sm btn-info">View
                                        Profile</a></div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="index.php"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">SKPD</span></li>

                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="setda.php"
                                aria-expanded="false"><img src="../assets/images/icon/setda/setda.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">Sekretariat Daerah
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dprd.php"
                                aria-expanded="false"><img src="../assets/images/icon/dprd/dprd.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">Sekretariat DPRD</span></a></li>
                                    <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                aria-expanded="false"><img src="../assets/images/icon/inspektorat/inspektorat-white.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">Inspektorat </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="inspektorat.php" class="sidebar-link"><span
                                            class="hide-menu"> SKPD
                                        </span>    
                             <li class="sidebar-item"><a href="inspektorat-kecamatan.php" class="sidebar-link"><span
                                            class="hide-menu"> Kecamatan
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="inspektorat-desa.php" class="sidebar-link"><span
                                            class="hide-menu"> Desa
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="disdikbud.php"
                                aria-expanded="false"><img src="../assets/images/icon/disdikbud/disdikbud.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISDIKBUD</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="dinkes.php"
                                aria-expanded="false"><img src="../assets/images/icon/dinkes/dinkes.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DINAS KESEHATAN</span></a>
                                    <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                    <li class="sidebar-item"><a href="dinkes.php" class="sidebar-link"><span
                                                class="hide-menu"> Infokes
                                            </span>    
                                 <li class="sidebar-item"><a href="dinkes-sigizi.php" class="sidebar-link"><span
                                                class="hide-menu"> Sigizi
                                            </span></a>
                                    </li>
                                </ul>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dputr.php"
                                aria-expanded="false"><img src="../assets/images/icon/dputr/dputr.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DPUTR-PRKPL</span></a></li>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="satpolpp.php"
                            aria-expanded="false"><img src="../assets/images/icon/satpolpp/satpolpp.svg" style="height: 30px;" class="feather-icon"></img><span
                                class="hide-menu">SATPOL PP</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dinsos.php"
                                aria-expanded="false"><img src="../assets/images/icon/dinsos/dinsos.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DINSOS PPKB P3A</span></a>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="disparpora.php"
                                aria-expanded="false"><img src="../assets/images/icon/disparpora/disparpora.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISPARPORA</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="diskopukmindag.php"
                                aria-expanded="false"><img src="../assets/images/icon/diskopukmindag/diskopukmindag.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISKOPUKMINDAG</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="disperpakan.php"
                                aria-expanded="false"><img src="../assets/images/icon/pertanian/pertanian.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISPERPAKAN</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="disdukcapil.php"
                                aria-expanded="false"><img src="../assets/images/icon/disdukcapil/disdukcapil.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISDUKCAPIL</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dpmptsp.php"
                                aria-expanded="false"><img src="../assets/images/icon/dpmptsp/dpmptsp.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DPMPTSPTK</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="dishubkominfo.php"
                                aria-expanded="false"><img src="../assets/images/icon/diskominfo/diskominfo.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">DISHUBKOMINFO</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="bappelitbangda.php"
                                aria-expanded="false"><img src="../assets/images/icon/bappeda/bappeda.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">BAPPELITBANGDA</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="bkpsdm.php"
                                aria-expanded="false"><img src="../assets/images/icon/bkpsdm/bkpsdm.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">BKPSDM</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="bpkpd.php"
                                aria-expanded="false"><img src="../assets/images/icon/bpkpd/bpkpd.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">BPKPD</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="bpbd.php"
                                aria-expanded="false"><img src="../assets/images/icon/bpbd/bpbd.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">BPBD</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="kesbangpol.php"
                                aria-expanded="false"><img src="../assets/images/icon/kesbangpol/kesbangpol.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">KESBANGPOL</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="smc.php"
                                aria-expanded="false"><img src="../assets/images/icon/smc/smc.svg" style="height: 30px;" class="feather-icon"></img><span
                                    class="hide-menu">RUMAH SAKIT SMC</span></a></li>
                            <br>
                            <br>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="table-basic.html" class="sidebar-link"><span
                                            class="hide-menu"> Basic Table
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="table-dark-basic.html" class="sidebar-link"><span
                                            class="hide-menu"> Dark Basic Table
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="table-sizing.html" class="sidebar-link"><span
                                            class="hide-menu">
                                            Sizing Table
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="table-layout-coloured.html" class="sidebar-link"><span
                                            class="hide-menu">
                                            Coloured
                                            Table Layout
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="table-datatable-basic.html" class="sidebar-link"><span
                                            class="hide-menu">
                                            Basic
                                            Datatables
                                            Layout
                                        </span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Inspektorat Daerah Kabupaten Tasikmalaya</h3>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="index.php" class="text-muted">Dashboard</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Data Inspektorat Daerah</li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">SKPD</li>
                                    <br>
                                    <br>
                                </ol>
                                <h6 class="text">Update Terakhir : Rabu, 14 Desember 2022 | Pukul 13.15 WIB</h6>
                            </nav>
                        </div>
                    </div>
                    <div class="col-5 align-self-center">
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Nilai Hasil Evaluasi Akuntabilitas Kinerja Perangkat Daerah</h4>
                                <h6 class="card-subtitle">Kabupaten Tasikmalaya Tahun 2020</h6>
                                <div class="table-responsive">
                                <table class="table">
                                        <thead class="bg-info text-white">
                                            <tr>
                                                <th>No.</th>
                                                <th>Perangkat Daerah</th>
                                                <th class="center" style="text-align:center">Nilai</th>
                                                <th class="center" style="text-align:center">Kategori</th>
                                                <th class="center" style="text-align:center">Interpretasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <?php 
                                            while($row = mysqli_fetch_array($result46))
                                            {
                                            echo '<tr>
                                                <td >'.$row['id'].'</td>
                                                <td>'.$row['Perangkat_Daerah'].'</td>
                                                <td style="text-align:center">'.$row['Nilai'].'</td>
                                                <td style="text-align:center">'.$row['Kategori'].'</td>
                                                <td style="text-align:center">'.$row['Interpretasi'].'</td>
                                            </tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                    <!-- column -->
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center text-muted">
                All Rights Reserved by Command Center Kabupaten Tasikmalaya. <br> Designed and Developed by <a
                    href="https://instagram.com/commandcenter_kabtasik">DISHUBKOMINFO</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="../dist/js/app-style-switcher.js"></script>
    <script src="../dist/js/feather.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <!-- themejs -->
    <!--Menu sidebar -->
    <script src="../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <!--Morris JavaScript -->
    <script src="../assets/libs/raphael/raphael.min.js"></script>
    <script src="../assets/libs/morris.js/morris.min.js"></script>
    <script src="../dist/js/pages/morris/morris-data.js"></script>
    <!-- Chart JS -->
    <script src="../dist/js/pages/chartjs/chartjs.init.js"></script>
    <script src="../assets/libs/chart.js/dist/Chart.min.js"></script>
    <!-- Script Chart Langsung -->
    <script>
        Morris.Bar({

            element : 'chart-cipatujah',
            data: [<?php echo $chart_data; ?>],
            xkey: 'Desa',
            ykeys: ['2019','2020'],
            labels: ['2019','2020'],
            barColors: ["#987554","#664229"],
            hideHover: 'auto',
            resize: true,
            gridTextSize: true,
            barRatio: 0.4,
            xLabelAngle: 0,

        });

         Morris.Bar({

            element : 'chart-pengunjung',
            data: [<?php echo $chart_data1; ?>],
            xkey: 'perpustakaan',
            ykeys: ['2020','2021'],
            labels: ['2020','2021'],
            hideHover: 'auto',
            resize: true,
            gridTextSize: 10,
            barRatio: 0.4,
            xLabelAngle: 0,
            // barColors: ["#987554","#664229"]

        });

        Morris.Donut({
            element: 'chart-diseminasi-informasi',
            data: [{
                label: "Press Release",
                value: 250,
            },{
                label: "Video",
                value: 66,
            }],
        resize: true,
        colors:['#5f76e8', '#8fa0f3'],
            
        });

        Morris.Bar({

            element : 'chart-jml',
            data: [<?php echo $chart_data3; ?>],
            xkey: 'jenis_perpustakaan',
            ykeys: ['column_2020','column_2021'],
            labels: ['column_2020','column_2021'],
            hideHover: 'auto',
            horizontal: true,
            resize: true,
            gridTextSize: 10,
            barRatio: 0.4,
            xLabelAngle: 60,
            // barColors: ["#987554","#664229"]

        });

        new Chart(document.getElementById("bar-chart-horizontal"), {
		type: 'horizontalBar',
		data: {
		  labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
		  datasets: [
			{
			  label: "Population (millions)",
			  backgroundColor: ["#6174d5", "#5f76e8", "#768bf4", "#7385df", "#b1bdfa"],
			  data: [8478,6267,5534,4784,3433]
			}
		  ]
		},
		options: {
		  legend: { display: false },
		  title: {
			display: true,
			text: 'Predicted world population (millions) in 2050'
		  }
		}
	});

    </script>
</body>

</html>